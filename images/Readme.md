# Docker Images

Each directory represents a Docker image.

The provided `Makefile` will build any directory and push the resulting image to
a GitLab container registry. For example:

```
make build-ci-runner
make push-ci-runner
```

## Images

### ci-runner

This image is used to launch the `helper.py` commands in a CI environment.

## Reference Images

The following images are used during the build process.

### artifact

This Dockerfile is used to build an image with a specific fuzz ready
configuration. The fuzz targets are compiled and the initial corpus is
generated.

### runner

This Dockerfile is used to build fully specified runner images.


