#!/usr/bin/env python

import sqlite3

CONN = None

run_table = """
CREATE TABLE IF NOT EXISTS runs (
    build_id TEXT,
    run_id INTEGER,
    execs INTEGER,
    coverage INTEGER,
    PRIMARY KEY (build_id, run_id)
);
"""

run_insert = """REPLACE INTO runs VALUES (?, ?, ?, ?)"""

build_table = """
CREATE TABLE IF NOT EXISTS builds (
    build_id TEXT PRIMARY KEY,
    target TEXT,
    engine TEXT,
    asan INTEGER
);
"""

build_insert = """REPLACE INTO builds VALUES (?, ?, ?, ?)"""

results_summary = """
SELECT b.target, b.build_id, b.engine, b.asan,avg(r.execs), avg(r.coverage), count(r.run_id)
FROM runs as r JOIN builds as b on r.build_id == b.build_id
GROUP BY b.build_id
ORDER BY target,engine,asan;
"""

def connect(path):
    global CONN
    if CONN is None:
        print("DB: connecting to {}".format(path))
        CONN = sqlite3.connect(path)
        #CONN.row_factory = sqlite3.Row
        ensure_tables()
    else:
        print("DB: connected")


def ensure_tables():
    tables = [run_table, build_table]
    for t in tables:
        CONN.execute(t)
    CONN.commit()

def store_run(run_data):
    CONN.execute(run_insert, run_data)
    CONN.commit()

def store_build(build_data):
    CONN.execute(build_insert, build_data)
    CONN.commit()

def query_stats():
    c = CONN.cursor()
    c.execute(results_summary)
    return  c.fetchall()
