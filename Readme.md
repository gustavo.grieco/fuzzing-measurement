# Fuzzing Measurement

Tooling to evaluate how various configuration parameters affect, or inform, the
design decisions for fuzzers.

*This repository contains submodules* in order to pull in fuzz targets such as
`oss-fuzz` and `fuzzer-test-suite`.
- you should use `git clone --recursive` or
- `git submodule update --init --recursive` if you have already cloned

## Quick Start

```
python helper.py ./experiments/test-oss-fuzz.yml build_experiment
```

## Details

The above command produces "fuzz ready" docker images for each possible
combination of the options and targets specified in the experimental
configuration. For example:

```
gcr.io/fuzz-measure/runner/harfbuzz_971e56ee
```

The configuration details for all builds are available:

```
build/test-oss-fuzz/experiment.yml
```

With a specific concrete configuration also available:

```
$ cat build/test-oss-fuzz/harfbuzz_2cba9fcd/configuration.yml
asan: false
engine: libfuzzer
target: harfbuzz
```

Using this running example, the `build_experiment` command essentially runs the
following stand alone commands for each possible concrete configuration.

```
export EXP=./experiments/test-oss-fuzz.yml
export CONF=./build/test-oss-fuzz/harfbuzz_b9012dab/configuration.yml
python helper.py $EXP build_project --no-pull --use-cache harfbuzz
python helper.py $EXP build_artifact $CONF
python helper.py $EXP build_runner $CONF
python helper.py $EXP test_runner $CONF
```

### build_project

Produces a docker image with a standard build environment for a given project.
This is created from the `Dockerfile` provided in each project directory and is
responsible for fetching the source code and installing any build dependencies.

Example: `gcr.io/fuzz-measure/project/harfbuzz`

### build_artifact

Produces a docker image with the fuzzing artifacts for a target built for
a specific fuzzing configuration (e.g. fuzzer, sanitizer, etc).

Example: `gcr.io/fuzz-measure/artifact/harfbuzz_b9012dab`

### build_runner

Copies the artifacts from the previous image into a common runner base image.

Example: `gcr.io/fuzz-measure/runner/harfbuzz_b9012dab`

### test_runner

Runs the `test_all`/`bad_build_check` scripts from `oss-fuzz` in the finalized
runner image.

### execute_runner

Launches a fuzz ready container and collects the results to the local disk.

```
build/test-oss-fuzz/
├── experiment.yml
└── json_834cb233
    ├── 0_results.tar.gz
    ├── configuration.yml
    └── env
```

### push_images

Uploads the finalized image to the container registry specified in the
experimental configuration for ease of distribution.


## Runing an experiment

Once the fuzz ready configurations are built the containers can be launched.
Typically this would be launched remotely with external orchestration, however
we provide convenience commands to run locally as well:

Execute an entire experiment locally

```
export EXP=./experiments/test-oss-fuzz.yml
python helper.py $EXP run_experiment
```

To run a single configuration.

```
export EXP=./experiments/test-oss-fuzz.yml
export CONF=./build/test-oss-fuzz/harfbuzz_b9012dab/configuration.yml
python helper.py $EXP execute_runner $CONF
```

## Process results

Once an experiment has been executed, the results from each run are stored as
tarballs, e.g.:

```
build/test-oss-fuzz/json_3dafaf6d/0_results.tar.gz
```

These can be processed to extract statistics and compute code coverage:

```
export EXP=./experiments/test-oss-fuzz.yml
python helper.py $EXP process_experiment
```

This will process the results into an sqlite database, e.g.:

```
build/test-oss-fuzz/results.db
```

This will also generate coverage reports for each run, e.g.:
```
build/test-oss-fuzz/json_3dafaf6d/run_0_report.tar.gz
```

## Other notes

Get a list of targets, each project may have more than one fuzz target:

```
docker run gcr.io/fuzz-measure/runner/json_3dafaf6d targets_list
parse_cbor_fuzzer
parse_afl_fuzzer
parse_ubjson_fuzzer
parse_msgpack_fuzzer
parse_bson_fuzzer
```

## CI example

This build can also be offloaded to CI with the `--ci` flag. Using a workflow
like the following to generate the configuration.

```
git checkout -b build-experiment
python helper.py build_experiment --ci ./experiments/test-oss-fuzz.yml
cp ./build/test-oss-fuzz/.gitlab-ci.yml .
git add .gitlab-ci.yml
git commit -m "Add CI for test-oss-fuzz experiment"
git push -u origin build-experiment
```

The results of an example [pipeline][p]. The pipeline will produce the
experimental configuration which can be [downloaded][d] or [browsed][b]. This
configuration has all the information necessary to utilize the generated
artifacts, e.g. generate image tags or identify a specific configuration.

[p]:https://gitlab.com/royragsdale/fuzzing-measurement/pipelines/71992326
[d]:https://gitlab.com/royragsdale/fuzzing-measurement/-/jobs/255591969/artifacts/download
[b]:https://gitlab.com/royragsdale/fuzzing-measurement/-/jobs/255591969/artifacts/browse
